// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

import VueLodash from 'vue-lodash'
import lodash from 'lodash'
import vueResource from 'vue-resource'
import {BootstrapVue, BContainer, IconsPlugin, BIcon, BIconArrowUp, BIconArrowDown} from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueGeoLocation from 'vue-browser-geolocation'
import * as VueGoogleMaps from 'vue2-google-maps'


// Install BootstrapVue
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.use(vueResource)
Vue.use(VueGeoLocation)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBJEoaOUZZN9RtdMVJ5JiveJM-IXyZDohU'
  },
  installComponents: true
})
Vue.use(require('vue-moment'))
Vue.use(VueLodash, { name: 'custom' , lodash: lodash })

Vue.component('b-container', BContainer)
Vue.component('BIcon', BIcon)
Vue.component('BIconArrowUp', BIconArrowUp)
Vue.component('BIconArrowDown', BIconArrowDown)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})
